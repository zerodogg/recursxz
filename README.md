recursxz
======

recursxz is a tool for recursively compressing files in a directory tree.
It can ignore already compressed files, and will uncompress a file it has
compressed if it turns out that the compression led to the file growing and
not shrinking.
